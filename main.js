const express = require('express')
const app = express()
const port = 7777

app.get('/', (req, res) => {
  res.send('Coucou!')
})

app.listen(port, () => {
  console.log(`Listening http://localhost:${port}`)
})